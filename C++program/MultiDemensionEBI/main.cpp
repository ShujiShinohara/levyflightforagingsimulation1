#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QVector>
#include <sstream>


#include "utility.h"
#include "MultiDemensionEBI.h"



#define TMAX 10000
#define TTT 10000
#define HYPOTHESES_SIZE 20 //Number of hypotheses
#define TRIMAX 100 //Number of trials

//#define PREY_DEV 0.05//Variance of prey distribution(scarse)
#define PREY_DEV 1.0//Variance of prey distribution（abundant）

//#define PREY_PATCH_SIZE 5 //Number of patches
#define PREY_PATCH_SIZE 1 //Number of patches

#define FOOD_CONSUMPTION 0.001 //Decrease in prey due to consumption

Utility *util;
MultiDemensionEBI *mdebi;


QVector< QVector<double> > preyMu;
QVector< QVector< QVector<double> >> preyDev;
QVector<double> preyW;

void createPreySite(int sitesize,int dimesnsion);

void create3DDat(QString filename,QVector< QVector<double> > mu, QVector< QVector< QVector<double> >> dev, QVector<double> P);

bool preySampling(QVector<double> data,bool isConsume, double *score);
int main(int argc, char *argv[])
{
    int tm=QTime::currentTime().msec();

    if(PREY_PATCH_SIZE==1)
    {
        tm=266;//when scarse
        //tm=281;//when abundant
    }
    else
    {
        tm=193;//when scarse
        //tm=483;//when abundant
    }

    qDebug()<<tm;
    srand( tm );

    QCoreApplication a(argc, argv);

    util=new Utility;


    int clustersize=HYPOTHESES_SIZE;
    int dimension=2;


    QVector< QVector<double> > hist_h;
    hist_h.clear();
    QVector< QVector<double> > hist_mu;
    hist_mu.clear();

    QVector<double> hist_correctRate;
    hist_correctRate.fill(0.0,TTT);

    QVector< double> rfreq1,rfreq2;
    QVector< QVector<double> > hist_result;



    QVector< QVector<double> > step_lengths;



    int tcounter=1;

    rfreq1.clear();
    rfreq2.clear();

    step_lengths.clear();

    for(int trial=0;trial<TRIMAX;trial++)
    {
        QVector<double> duration;
        duration.clear();
        QVector<double> step_length;
        step_length.clear();

        createPreySite(PREY_PATCH_SIZE,dimension);
        if(trial==TRIMAX-1)
        {
            create3DDat("normalDist.dat",preyMu,preyDev,preyW);
            qDebug()<<preyMu;
        }



        mdebi=NULL;
        mdebi=new MultiDemensionEBI(clustersize,dimension);


        qDebug()<<"trial="<<trial;


        hist_result.clear();

        int pre_hmax=0;
        QVector<double> premu;
        premu.fill(0.0,dimension);

        QVector<double> predat;
        predat.fill(0.0,dimension);



        hist_h.clear();
        hist_mu.clear();
        hist_result.clear();

        QVector<double> dat;

        dat.fill(0.0,dimension);



        double sum_counter=0.0;
        int tt=0;
        for(int t=0;t<TMAX;t++)
        {

            dat=mdebi->sampling();
            double score=0.0;
            bool isConsume=true;

            if(PREY_PATCH_SIZE==1)
                isConsume=false;


            bool flag=preySampling(dat,isConsume,&score);
            if(flag)
            {
                if(PREY_PATCH_SIZE==1)
                    mdebi->onlineInference(dat,flag);//Agents do not learn when they cannot acquire prey.

                if(t>=TMAX-TTT)
                    sum_counter++;
            }

            if(PREY_PATCH_SIZE>1)
                mdebi->onlineInference(dat,flag);//When agents cannot acquire prey, they learn negatively.

            if(t<TMAX-TTT)
                continue;

            hist_correctRate[tt]+=score/(double)TRIMAX;



            double distance1=0.0;
            double distance2=0.0;
            double distance3=0.0;
            distance1=util->calcL2Norm(mdebi->hmaxMu().toStdVector(),premu.toStdVector());//Distance between the current estimated centre and the previous estimated centre
            distance2=util->calcL2Norm(dat.toStdVector(),predat.toStdVector());//step length
            distance3=util->calcL2Norm(mdebi->hmaxMu().toStdVector(),preyMu[0].toStdVector());//Distance between the estimated centre and the actual centre of prey distribution

            if(mdebi->hmax()==pre_hmax)
                tcounter++;
            else
            {
                duration.append((double)tcounter);
                tcounter=1;
            }

            rfreq1.append(distance1);
            rfreq2.append(distance2);

            step_length.append(distance2);

            premu=mdebi->hmaxMu();

            predat=dat;

            pre_hmax=mdebi->hmax();

            if(trial==TRIMAX-1)
            {
                QVector<double> tmp;
                QVector<double> tmp1;

                tmp.clear();
                tmp.append(preyMu[0]);
                tmp.append(dat);

                tmp.append(mdebi->hmaxMu());

                tmp.append(mdebi->hmax());

                tmp.append(mdebi->hmaxDev().at(0).at(0));

                if(dimension>1)
                    tmp.append(mdebi->hmaxDev().at(1).at(1));

                tmp.append(distance3);

                tmp.append(flag);
                tmp.append(mdebi->hmaxCH());
                tmp.append(score);
                tmp.append(distance2);


                hist_result.append(tmp);


                tmp1.clear();

                QVector< QVector<double> > tmpmu=mdebi->mu();

                for(int k=0;k<mdebi->ch().size();k++)
                {
                    for(int d=0;d<dimension;d++)
                    {
                        tmp1.append(tmpmu[k][d]);

                    }
                }

                hist_h.append(mdebi->ch());
                hist_mu.append(tmp1);

            }

            tt++;
        }//t

        qDebug()<<sum_counter/(double)tt;

        step_lengths.append(step_length);



        QString fname=QString("step_lengths")+QString::number(trial)+QString(".csv");
        util->save1(step_length,fname);

        //        fname=QString("duration")+QString::number(trial)+QString(".csv");
        //        util->save1(duration,fname);


    }//trial

    rfreq1=QVector<double>::fromStdVector(util->sort_vector(rfreq1.toStdVector(),false));
    rfreq2=QVector<double>::fromStdVector(util->sort_vector(rfreq2.toStdVector(),false));


    QVector<QVector<double>> rfreq;
    rfreq.clear();
    QVector<double> tmp;
    tmp.clear();

    int ct=0;
    for(int i=0;i<rfreq1.size();i++)
    {
        if(i%TRIMAX==0)
        {
            tmp.clear();
            tmp.append(rfreq1[i]);
            tmp.append(rfreq2[i]);
            tmp.append(hist_correctRate[ct]);

            rfreq.append(tmp);
            ct++;
        }
    }


    util->save2(rfreq,"rfreq.csv");

    util->save2(hist_h,"hist_h.csv");
    util->save2(hist_mu,"hist_mu.csv");
    util->save2(hist_result,"hist_result.csv");



    qDebug()<<"End!!";

    return a.exec();


}





void createPreySite(int sitesize,int dimension)
{

    preyMu.resize(sitesize);
    preyDev.resize(preyMu.size());
    preyW.fill(0.0,preyMu.size());

    for(int k=0;k<preyMu.size();k++)
    {
        preyMu[k].fill(0.0,dimension);
        preyDev[k].resize(dimension);
        for(int d=0;d<dimension;d++)
        {
            preyDev[k][d].fill(0.0,dimension);
        }
    }



    for(int k=0;k<sitesize;k++)
    {

        util->drnd();
        double xx=util->drnd();
        double yy=util->drnd();

        preyMu[k][0]=-fabs(RANGE)+2.0*fabs(RANGE)*xx;
        preyMu[k][1]=-fabs(RANGE)+2.0*fabs(RANGE)*yy;


        preyW[k]=1.0;
        for(int d1=0;d1<dimension;d1++)
        {
            for(int d2=0;d2<dimension;d2++)
            {
                if(d1==d2)
                {
                    preyDev[k][d1][d2]=PREY_DEV*fabs(RANGE);
                }
            }

        }
    }

}

bool preySampling(QVector<double> data,bool isConsume,double *score)
{

    QVector<double> val1;
    val1.fill(0.0,preyDev.size());
    *score=0.0;

    double sum=0.0;
    for(int k=0;k<preyDev.size();k++)
    {

        val1[k]=util->Gaussian1(data,preyMu[k],preyDev[k])*preyW[k];

        sum+=val1[k];
    }
    bool flag=true;

    if(std::isinf(sum) || std::isnan(sum))
    {
        qDebug()<<"score error!";
        qDebug()<<"data="<<data;
        qDebug()<<"prey Mu="<<preyMu;
        qDebug()<<"prey Dev="<<preyDev;
        qDebug()<<"prey Weight="<<preyW;
        *score=0.0;
        return false;
    }

    *score=sum;
    sum=sum*1.0;
    if(util->drnd()>sum)
        flag=false;


    if(isConsume)
    {
        if(flag)
        {


            for(int k=0;k<preyW.size();k++)
            {
                preyW[k]-=FOOD_CONSUMPTION*(val1[k]/sum);

                preyW[k]=std::max(preyW[k],0.0);
            }
        }
    }


    return flag;
}


void create3DDat(QString filename,QVector< QVector<double> > mu, QVector< QVector< QVector<double> >> dev, QVector<double> P)
{
    QVector< QVector<double> > dist;
    dist.resize(100);
    for(int i=0;i<dist.size();i++)
    {
        dist[i].resize(dist.size());
    }

    QVector<double> data;
    data.resize(2);
    double delta=2.0*fabs(RANGE)/(double)dist.size();

    for(int i=0;i<dist.size();i++)
    {

        for(int j=0;j<dist[i].size();j++)
        {
            double x=-fabs(RANGE)+(double)i*delta;
            double y=-fabs(RANGE)+(double)j*delta;

            data[0]=x;
            data[1]=y;

            double sum=0.0;
            for(int k=0;k<dev.size();k++)
            {

                double val1=util->Gaussian1(data,mu[k],dev[k])*P[k];
                sum+=val1;
            }


            dist[i][j]=sum;
        }
    }

    util->save3D(dist,filename,-fabs(RANGE),-fabs(RANGE),2.0*fabs(RANGE)/100.0,2.0*fabs(RANGE)/100.0);



}





