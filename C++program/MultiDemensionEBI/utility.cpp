﻿#include "utility.h"
using namespace std;


Utility::Utility()
{
}

Utility::~Utility()
{
}

double Utility::Gaussian(double x,double ave,double dev)
{
    double val=0.0;

    val=1.0/sqrt(2.0*M_PI*dev)*exp(-(x-ave)*(x-ave)/(2.0*dev));
    return val;

}
QVector< QVector<double> > Utility::MatrixMultiplication(QVector< QVector<double> > xx,QVector< QVector<double> >yy)
{
    QVector< QVector<double> > output;
    int nn=yy.size();


    if(xx[0].size()!=yy.size())
    {
        qDebug()<<"MatrixMultiplication size error!! "<<xx[0].size()<<" "<<yy.size();
        return output;
    }

    output.resize(xx.size());
    for(int i=0;i<xx.size();i++)
    {
        output[i].resize(yy[0].size());
    }


    for(int i=0;i<output.size();i++)
    {
        for(int j=0;j<output[i].size();j++)
        {
            double sum=0.0;
            for(int k=0;k<nn;k++)
            {
                sum+=xx[i][k]*yy[k][j];
            }

            output[i][j]=sum;

        }
    }

    return output;
}
double Utility::Gaussian(QVector<double> x,QVector<double> mu,QVector< QVector<double> > dev)
{
    double detdev;
    QVector< QVector<double> > invdev;

    invdev=this->calcInverseMatrix2(dev,&detdev);

    double val=0.0;
    int nn=x.size();

    if(nn!=mu.size())
    {
        qDebug()<<"Gaussian size error1!!";
        qDebug()<<"nn="<<nn<<" mu.size()="<<mu.size();
        return val;
    }
    if(nn!=invdev.size() || nn!=invdev[0].size())
    {
        qDebug()<<"Gaussian size error2!!";
        return val;
    }

    QVector< QVector<double> > xx,xxT;
    xx.clear();
    xxT.clear();
    QVector<double> bb;
    bb.clear();
    for(int i=0;i<nn;i++)
    {
        QVector<double> aa;
        aa.clear();

        double val=x[i]-mu[i];
        aa.append(val);
        bb.append(val);
        xx.append(aa);
    }
    xxT.append(bb);


    QVector< QVector<double> > tmp=this->MatrixMultiplication(invdev,xx);
    tmp=this->MatrixMultiplication(xxT,tmp);

    if(tmp.size()!=1 || tmp[0].size()!=1)
    {
        qDebug()<<"Gaussian size error3!!"<<tmp.size()<<" "<<tmp[0].size();
        return val;
    }

    val=exp(-0.5*(tmp[0][0]))/(sqrt(pow(2.0*M_PI,(double)nn)*fabs(detdev)));
    return val;

}
double Utility::Gaussian1(QVector<double> x,QVector<double> mu,QVector< QVector<double> > dev)
{
    double detdev;
    QVector< QVector<double> > invdev;

    invdev=this->calcInverseMatrix2(dev,&detdev);

    double val=0.0;
    int nn=x.size();

    if(nn!=mu.size())
    {
        qDebug()<<"Gaussian size error1!!";
        qDebug()<<"nn="<<nn<<" mu.size()="<<mu.size();
        return val;
    }
    if(nn!=invdev.size() || nn!=invdev[0].size())
    {
        qDebug()<<"Gaussian size error2!!";
        return val;
    }

    QVector< QVector<double> > xx,xxT;
    xx.clear();
    xxT.clear();
    QVector<double> bb;
    bb.clear();
    for(int i=0;i<nn;i++)
    {
        QVector<double> aa;
        aa.clear();

        double val=x[i]-mu[i];
        aa.append(val);
        bb.append(val);
        xx.append(aa);
    }
    xxT.append(bb);


    QVector< QVector<double> > tmp=this->MatrixMultiplication(invdev,xx);
    tmp=this->MatrixMultiplication(xxT,tmp);

    if(tmp.size()!=1 || tmp[0].size()!=1)
    {
        qDebug()<<"Gaussian size error3!!"<<tmp.size()<<" "<<tmp[0].size();
        return val;
    }

    val=exp(-0.5*(tmp[0][0]));
    return val;

}
double Utility::Gaussian(QVector<double> x,QVector<double> mu,double detdev, QVector< QVector<double> > invdev)
{
    double val=0.0;
    int nn=x.size();

    if(nn!=mu.size())
    {
        qDebug()<<"Gaussian size error1!!";
        qDebug()<<"nn="<<nn<<" mu.size()="<<mu.size();
        return val;
    }
    if(nn!=invdev.size() || nn!=invdev[0].size())
    {
        qDebug()<<"Gaussian size error2!!";
        return val;
    }




    QVector< QVector<double> > xx,xxT;
    xx.clear();
    xxT.clear();
    QVector<double> bb;
    bb.clear();
    for(int i=0;i<nn;i++)
    {
        QVector<double> aa;
        aa.clear();

        double val=x[i]-mu[i];
        aa.append(val);
        bb.append(val);
        xx.append(aa);
    }
    xxT.append(bb);


    QVector< QVector<double> > tmp=this->MatrixMultiplication(invdev,xx);
    tmp=this->MatrixMultiplication(xxT,tmp);

    if(tmp.size()!=1 || tmp[0].size()!=1)
    {
        qDebug()<<"Gaussian size error3!!"<<tmp.size()<<" "<<tmp[0].size();
        return val;
    }

    val=exp(-0.5*(tmp[0][0]))/(sqrt(pow(2.0*M_PI,(double)nn)*fabs(detdev)));
    return val;
}


QVector< QVector<double> > Utility::calcInverseMatrix2(QVector< QVector<double> > input, double *det)
{
    QVector< QVector<double> > output;
    output.clear();
    output.resize(2);
    for(int i=0;i<2;i++)
    {
        output[i].fill(0.0,2);
    }

    double tmp=input[0][0]*input[1][1]-input[0][1]*input[1][0];
    if(tmp!=0.0)
    {
        output[0][0]=input[1][1]/tmp;
        output[1][1]=input[0][0]/tmp;
        output[0][1]=-input[0][1]/tmp;
        output[1][0]=-input[1][0]/tmp;
    }
    *det=tmp;
    return output;

}



double Utility::calcL2Norm(std::vector<double> data,std::vector<double> center)
{
    double sum=0.0;

    double rt=0.0;
    if(data.size()!=center.size())
        return rt;

    for(int i=0;i<data.size();i++)
    {
        sum+=std::pow((data.at(i)-center.at(i)),2.0);
    }


    rt= sqrt(sum);

    return rt;
}



double Utility::drnd()
{
    return ((double)qrand() / ((double)RAND_MAX + 1));
}

int Utility::argmax(QVector< double> data)
{
    int index=0;
    if(data.size()==0)
        return index;

    QVector<int> tmp;
    tmp.clear();
    double max=data.at(0);
    tmp.append(0);

    for(int i=1;i<data.size();i++)
    {
        if(max<data.at(i))
        {
            tmp.clear();
            tmp.append(i);
            max=data.at(i);
        }
        else if(max==data.at(i))
        {
            tmp.append(i);

        }
    }
    index=tmp.at((int)((double)tmp.size()*drnd()));
    return index;

}


int Utility::argmin(QVector< double> data)
{
    int index=0;
    if(data.size()==0)
        return index;

    double min=data.at(0);
    for(int i=1;i<data.size();i++)
    {
        if(min>data.at(i))
        {
            index=i;
            min=data.at(i);
        }
    }

    return index;

}


void Utility::save1(QVector<double> ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        ss<<ary[i]<<std::endl;
    }

    out << ss.str().c_str();
    file.close();

}
void Utility::save1(QVector<int> ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        ss<<ary[i]<<std::endl;
    }

    out << ss.str().c_str();
    file.close();

}

void Utility::save2D(QVector<double> ary, QString filename)
{
    QFile file(filename);

    std::string sep=" ";
    QFileInfo info(filename);
    if(info.suffix()=="csv")
        sep=",";


    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        ss<<i<<sep<<ary[i]<<std::endl;
    }

    out << ss.str().c_str();
    file.close();

}

void Utility::save3D(QVector< QVector<double> > ary, QString filename,double startx,double starty,double divx,double divy)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    std::string sep=" ";
    QFileInfo info(filename);
    if(info.suffix()=="csv")
        sep=",";

    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        for(int j=0;j<ary[i].size();j++)
        {
            ss<<startx+(double)i*divx<<sep<<starty+(double)j*divy<<sep<< ary[i][j]<<std::endl;

        }
        ss<<std::endl;
    }

    out << ss.str().c_str();
    file.close();


}

void Utility::save2(QVector< QVector<double> > ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    std::string sep=" ";
    QFileInfo info(filename);
    if(info.suffix()=="csv")
        sep=",";

    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        for(int j=0;j<ary[i].size();j++)
        {
            if(j>0)
                ss<<sep<<ary[i][j];
            else
                ss<<ary[i][j];
        }
        ss<<std::endl;

    }

    out << ss.str().c_str();
    file.close();

}
QVector< double> Utility::normalize(QVector< double> data)
{

    QVector< double> rt;
    rt.clear();
    double sum1=this->sum(data);

    for(int i=0;i<data.size();i++)
    {
        if(sum1!=0.0)
        {
            rt.append(data.at(i)/sum1);
        }
        else
        {
            rt.append(1.0/(double)data.size());

        }
    }


    return rt;
}
double Utility::sum(QVector<double> dat)
{
    double sum=0.0;
    for(int i=0;i<dat.size();i++)
    {
        sum+=dat.at(i);
    }
    return sum;
}



std::vector<double> Utility::sort_vector(std::vector<double> input,bool isAscending)
{
    std::vector<double> output;
    std::copy(input.begin(), input.end(), back_inserter(output) );


    if(isAscending)
        std::sort(output.begin(),output.end());
    else
        std::sort(output.begin(),output.end(),std::greater<double>());

    return output;
}


QVector< QVector<double> > Utility::choleskyDecomp(QVector< QVector<double> > m)
{
    QVector< QVector<double>> l;
    l.clear();

    if(m.size() <= 0 || m.size() != m[0].size()) {
        return l;
    }

    int n=m.size();
    int i, j, k;

    for(i = 1; i < n; i++) {
        for(j = 0; j < i; j++) {
            if(m[i][j] != m[j][i]) {
                //                qDebug()<<"choleskyDecomp error";
                m[i][j] = m[j][i];
                //                return l;
            }
        }
    }

    double ld, lld;
    QVector<double> d;
    d.resize(n);

    l.resize(n);

    for(i = 0; i < n; i++) {
        l[i].resize(i+1);
    }

    l[0][0] = 1;
    d[0] = m[0][0];

    for(i = 0; i < n; i++) {
        for(j = 0; j < i; j++) {
            lld = m[i][j];
            for(int k = 0; k < j; k++) {
                lld -= l[i][k] * l[j][k] * d[k];
            }
            l[i][j] = lld / d[j];
        }

        ld = m[i][i];
        for(k = 0; k < i; k++) {
            ld -= l[i][k] * l[j][k] * d[k];
        }
        l[i][j] = 1;
        d[i] = ld;
    }

    for(j = 0; j < n; j++) {
        d[j] = sqrt(d[j]);
    }

    for(i = 0; i < n; i++) {
        for(j = 0; j <= i; j++) {
            l[i][j] *= d[j];
        }
    }
    return l;
}

QVector<double> Utility::randGaussian( QVector<double> mu, QVector< QVector<double> > dev )
{

    int dim=dev.size();

    QVector<double> rt;
    rt.fill(0.0,dim);

    QVector< QVector<double> > lower_tri_matrix;
    if(mu.size()!=dev.size())
    {
        qDebug()<<"randGaussian error!";
        return rt;
    }
    if(mu.size()!=dev[0].size())
    {
        qDebug()<<"randGaussian error!";
        return rt;
    }

    QVector<double> Z;
    Z.clear();

    for(int i=0;i<dim;i++)
    {
        Z.append(randGaussian(0.0,1.0));
    }

    lower_tri_matrix=this->choleskyDecomp(dev);


    for(int i=0;i<dim;i++)
    {
        rt[i]=mu[i];
        for(int j=0;j<=i;j++)
        {
            rt[i]+=lower_tri_matrix[i][j]*Z[j];
        }

    }

    return rt;
}

QVector<double> Utility::randGaussian2D( QVector<double> mu, QVector< QVector<double> > dev )
{

    int dim=2;

    QVector<double> rt;
    rt.fill(0.0,dim);

    double eps1=this->randGaussian(0.0,1.0);
    double eps2=this->randGaussian(0.0,1.0);

    double sx=dev[0][0];
    double sy=dev[1][1];
    double sxy=dev[0][1];
    double a=sxy/(sx*sx);
    double b=sqrt(sy*sy-sxy*sxy/(sx*sx));
    rt[0]=mu[0]+dev[0][0]*eps1;
    rt[1]=mu[1]+a*(rt[0]-mu[0])+b*eps2;

    QVector< QVector<double> > lower_tri_matrix;
    if(mu.size()!=dev.size())
    {
        qDebug()<<"randGaussian error!";
        return rt;
    }
    if(mu.size()!=dev[0].size())
    {
        qDebug()<<"randGaussian error!";
        return rt;
    }

    QVector<double> Z;
    Z.clear();

    for(int i=0;i<dim;i++)
    {
        Z.append(randGaussian(0.0,1.0));
    }

    lower_tri_matrix=this->choleskyDecomp(dev);


    for(int i=0;i<dim;i++)
    {
        rt[i]=mu[i];
        for(int j=0;j<=i;j++)
        {
            rt[i]+=lower_tri_matrix[i][j]*Z[j];
        }

    }

    return rt;
}

double Utility::randGaussian( double mu, double sigma ){
    double z=sqrt( -2.0*log(Uniform()) ) * sin( 2.0*M_PI*Uniform() );
    return mu + sigma*z;
}

double Utility::Uniform( void ){
    return ((double)rand()+1.0)/((double)RAND_MAX+2.0);
}

void swapi(int *a, int *b)
{
    int w;

    w = *a;
    *a = *b;
    *b = w;
    return;
}
void swapd(double *a, double *b)
{
    double w;

    w = *a;
    *a = *b;
    *b = w;
    return;
}



