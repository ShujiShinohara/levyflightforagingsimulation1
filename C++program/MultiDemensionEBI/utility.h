﻿#ifndef Utility_H
#define Utility_H


#include <string>
#include <float.h>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QVector>
#include <QFileInfo>
#include <sstream>
#include <random>
#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <map>
#include <functional>
#include <string.h>


class Utility
{
public:

    Utility();
    ~Utility();

    QVector<double> randGaussian2D( QVector<double> mu, QVector< QVector<double> > dev );
    int argmax(QVector< double>);
    int argmin(QVector< double>);
    double drnd();
    QVector< double> normalize(QVector< double> data);
    QVector< QVector<double> > calcInverseMatrix2(QVector< QVector<double> > input, double *det);
    double Gaussian(QVector<double> x,QVector<double> mu,double detdev, QVector< QVector<double> > invdev);
    double Gaussian(QVector<double> x,QVector<double> mu,QVector< QVector<double> > dev);
    double calcL2Norm(std::vector<double> data,std::vector<double> center);
    void save1(QVector<double> ary, QString filename);
    void save1(QVector<int> ary, QString filename);
    void save2(QVector< QVector<double> > ary, QString filename);
    std::vector<double> sort_vector(std::vector<double> input,bool isAscending=true);
    void save2D(QVector<double> ary, QString filename);
    void save3D(QVector< QVector<double> > ary, QString filename,double startx=0.0,double starty=0.0,double divx=1.0,double divy=1.0);
    double randGaussian( double mu, double sigma );
    QVector<double> randGaussian( QVector<double> mu, QVector< QVector<double> > dev );
    double sum(QVector<double> dat);
    QVector< QVector<double> > MatrixMultiplication(QVector< QVector<double> > xx,QVector< QVector<double> >yy);

    double Uniform( void );
    double Gaussian(double x,double ave,double dev);
    double Gaussian1(QVector<double> x,QVector<double> mu,double detdev, QVector< QVector<double> > invdev);
    double Gaussian1(QVector<double> x,QVector<double> mu,QVector< QVector<double> > dev);


private:

    QVector< QVector<double> > choleskyDecomp(QVector< QVector<double> > m);

};

#endif // Utility_H
