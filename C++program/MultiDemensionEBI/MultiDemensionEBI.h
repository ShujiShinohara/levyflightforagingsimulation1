#ifndef BIBAGENT_H
#define BIBAGENT_H


#define RANGE 5.0 //Range of center of model
#define MODEFLAG 2 //0:SDEM、1:BIB、2:BID

#define VARIANCE 0.5 //Variance of model

#define EPS_H 10E-80 //Minimum value of each hypothesis confidence
#define EPSII 10E-80//Minimum value to prevent division by zero

#define ALPHA 0.03 //learning rate
#define BETA 0.03//didcounting rate


#include <QDebug>
#include <QVector>
#include <sstream>


#include "utility.h"


class MultiDemensionEBI
{
public:
    MultiDemensionEBI();
    MultiDemensionEBI(int clustersize, int dimension=1);


    void initialize();

    QVector<double> sampling();
    int hmax();
    QVector<double> hmaxMu();
    double hmaxCH();
    QVector< QVector<double> > hmaxDev();

    int hmin();
    QVector<double> hminMu();
    double hminCH();
    QVector< QVector<double> > hminDev();

    QVector< QVector<double> > mu();
    QVector< QVector< QVector<double> > > dev();
    QVector<double> ch();

    void onlineInference(QVector<double> data,bool flag=true);


    QVector<double> alpha();
    QVector<double> beta();


private:

    QVector<double> m_alpha;
    QVector<double> m_beta;
    Utility *util;
    int m_dimension;
    int m_clustersize;

    int m_datasize;


    QVector< QVector<double> > m_mu;
    QVector< QVector< QVector<double> > > m_dev;
    QVector<double> m_ch;


    QVector< QVector<double> > m_data;

    QVector< QVector<double> > m_pdh;
    QVector< QVector<double> > m_phd;//尤度

    int m_hmax;
    int m_hmin;

    QVector< QVector<double> > calcOnlineMu(QVector<double> data);
    QVector<double> calcOnlineH(QVector <double> data, bool flag=true);

    QVector< QVector< QVector<double> > > calcOnlineDev(QVector<double> data);

    void calcPDH(QVector<double> data, int t,bool flag=true);

    QVector<double> calcDeltaMu(QVector<double> data, int k,int t);

    QVector< QVector<double> > calcDeltaDev(QVector<double> data,int k);


};

#endif // BIBAGENT_H
