QT += core
QT -= gui

CONFIG += c++11

TARGET = InverseBayes
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    MultiDemensionEBI.cpp \
    utility.cpp

HEADERS += \
    MultiDemensionEBI.h \
    utility.h
