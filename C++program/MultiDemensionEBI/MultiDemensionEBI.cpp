#include "MultiDemensionEBI.h"

MultiDemensionEBI::MultiDemensionEBI()
{
    util=new Utility;

    m_clustersize=1;
    m_dimension=1;
    m_datasize=1;
    m_hmax=0;

    initialize();
}

MultiDemensionEBI::MultiDemensionEBI(int clustersize,int dimension)
{
    util=new Utility;

    m_clustersize=clustersize;

    if(MODEFLAG==0)
        m_clustersize=1;

    m_dimension=dimension;
    m_datasize=1;

    initialize();
}

QVector<double> MultiDemensionEBI::sampling()
{
    QVector<double> data;
    data.fill(0.0,m_dimension);

    data=util->randGaussian2D(this->hmaxMu(),this->hmaxDev());
    if(std::isinf(data[0]) || std::isnan(data[1]))
    {
        qDebug()<<"dat error!";
        qDebug()<<"data="<<data;
        qDebug()<<"Mu="<<this->hmaxMu();
        qDebug()<<"Dev="<<this->hmaxDev();
        data.fill(0.0,m_dimension);


    }
    return data;

}
QVector<double> MultiDemensionEBI::alpha()
{
    return m_alpha;
}
QVector<double> MultiDemensionEBI::beta()
{
    return m_beta;
}
int MultiDemensionEBI::hmax()
{

    m_hmax=util->argmax(m_ch);
    return std::max(0,std::min(m_clustersize-1,m_hmax));
}

int MultiDemensionEBI::hmin()
{

    m_hmin=util->argmin(m_ch);
    return std::max(0,std::min(m_clustersize-1,m_hmin));
}
QVector<double> MultiDemensionEBI::hmaxMu()
{

    return m_mu[this->hmax()];
}
double MultiDemensionEBI::hmaxCH()
{

    return m_ch[this->hmax()];
}

QVector< QVector<double> > MultiDemensionEBI::hmaxDev()
{

    return m_dev[this->hmax()];

}

QVector<double> MultiDemensionEBI::hminMu()
{

    return m_mu[this->hmin()];
}
double MultiDemensionEBI::hminCH()
{

    return m_ch[this->hmin()];
}

QVector< QVector<double> > MultiDemensionEBI::hminDev()
{

    return m_dev[this->hmin()];

}



QVector< QVector<double> > MultiDemensionEBI::mu()
{
    return m_mu;
}
QVector< QVector< QVector<double> > > MultiDemensionEBI::dev()
{
    return m_dev;
}
QVector<double> MultiDemensionEBI::ch()
{
    return m_ch;
}


void MultiDemensionEBI::initialize()
{
    m_hmax=0;
    m_alpha.fill(ALPHA,m_clustersize);
    m_beta.fill(BETA,m_clustersize);

    m_data.clear();


    m_mu.clear();
    m_dev.clear();

    m_mu.resize(m_clustersize);
    m_dev.resize(m_clustersize);
    for(int k=0;k<m_clustersize;k++)
    {
        m_mu[k].fill(0.0,m_dimension);
        m_dev[k].resize(m_dimension);
        for(int i=0;i<m_dimension;i++)
        {
            m_dev[k][i].fill(0.0,m_dimension);
        }
    }



    for(int k=0;k<m_clustersize;k++)
    {
        double rr=fabs(RANGE);
        for(int d=0;d<m_dimension;d++)
        {
            m_mu[k][d]=-rr+2.0*rr*util->drnd();
        }

        for(int i=0;i<m_dimension;i++)
        {
            for(int j=0;j<m_dimension;j++)
            {
                m_dev[k][i][j]=0.0;
                if(i==j)
                {

                    m_dev[k][i][j]=VARIANCE;
                }

            }
        }

    }

    m_ch.clear();
    m_ch.fill(1.0/(double)m_clustersize,m_clustersize);

}

void MultiDemensionEBI::onlineInference(QVector<double> data, bool flag)
{
    m_phd.clear();
    m_phd.resize(m_clustersize);
    for(int i=0;i<m_phd.size();i++)
    {
        m_phd[i].fill(0.0,1);
    }
    m_pdh.clear();
    m_pdh.resize(1);
    for(int i=0;i<m_pdh.size();i++)
    {
        m_pdh[i].fill(0.0,m_clustersize);
    }
    for(int i=0;i<m_pdh.size();i++)
    {
        for(int k=0;k<m_pdh[i].size();k++)
        {
            m_pdh[i][k]=m_ch[k];

        }

    }


    this->calcOnlineH(data,flag);

    if(!flag)
        return;

    if(MODEFLAG!=2)
    {
        QVector< QVector<double> > deltamu;
        QVector< QVector< QVector<double> > > deltadev;

        deltamu=this->calcOnlineMu(data);
        deltadev=this->calcOnlineDev(data);

        for(int k=0;k<m_clustersize;k++)
        {
            for(int d=0;d<m_dimension;d++)
            {
                m_mu[k][d]+=deltamu[k][d];
            }
        }



        for(int k=0;k<m_clustersize;k++)
        {
            for(int d0=0;d0<m_dimension;d0++)
            {
                for(int d1=0;d1<m_dimension;d1++)
                {
                    m_dev[k][d0][d1]+=deltadev[k][d0][d1];
                    double min=std::max(fabs(m_dev[k][d0][d1]),EPSII);
                    if(m_dev[k][d0][d1]>=0.0)
                        m_dev[k][d0][d1]=min;
                    else
                        m_dev[k][d0][d1]=-min;
                }
            }
        }
    }



}

QVector<double> MultiDemensionEBI::calcOnlineH(QVector <double> data,bool flag)
{
    calcPDH(data,0,flag);

    QVector<double> deltah;
    deltah.fill(0.0,m_clustersize);
    for(int k=0;k<m_clustersize;k++)
    {
        deltah[k]=m_pdh[0][k]-m_ch[k];
        m_ch[k]=m_pdh[0][k];
    }

    m_hmax=util->argmax(m_ch);
    return deltah;
}

QVector< QVector< QVector<double> > > MultiDemensionEBI::calcOnlineDev(QVector<double> data)
{
    QVector< QVector< QVector<double> > > deltadev;
    deltadev.clear();
    deltadev.resize(m_clustersize);
    for(int k=0;k<m_clustersize;k++)
    {
        deltadev[k].resize(m_dimension);
        for(int i=0;i<m_dimension;i++)
        {
            deltadev[k][i].fill(0.0,m_dimension);
        }
    }


    deltadev[m_hmax]=calcDeltaDev(data,m_hmax);


    return deltadev;
}

QVector< QVector<double> > MultiDemensionEBI::calcOnlineMu(QVector<double> data)
{
    QVector< QVector<double> > deltamu;
    deltamu.resize(m_clustersize);
    for(int k=0;k<m_clustersize;k++)
    {
        deltamu[k].fill(0.0,m_dimension);
    }



    deltamu[m_hmax]=calcDeltaMu(data,m_hmax,0);

    return deltamu;
}

void MultiDemensionEBI::calcPDH(QVector<double> data, int t, bool flag)
{
    double delta1=pow(sqrt(2.0*M_PI),(double)m_dimension);
    double delta=0.0;

    for(int k=0;k<m_clustersize;k++)
    {

        double f0=0.0;

        double detdev=0.0;
        QVector< QVector<double> > inv;
        if(m_dimension==1)
        {
            inv.resize(1);
            inv[0].fill(0.0,1);
            inv[0][0]=1.0/m_dev[k][0][0];
            detdev=m_dev[k][0][0];
        }
        else if(m_dimension==2)
        {
            inv=util->calcInverseMatrix2(m_dev[k],&detdev);


            if(detdev==0.0)
            {
                qDebug()<<"calcInverseMatrix2 error!! "<<m_dev[k]<<" "<<m_alpha[k];
                return;
            }
        }


        delta=delta1*sqrt(fabs(detdev));
        double val1=util->Gaussian(data,m_mu[k],detdev,inv)*delta;

        if(val1>1.0 || val1<0.0 )
        {
            qDebug()<<"value error";
            qDebug()<<val1;
            qDebug()<<data;
            qDebug()<<detdev;
            qDebug()<<m_mu[k];
            qDebug()<<inv;
            qDebug()<<"\n\n";
            return;
        }
        else
        {
            f0=std::max(EPSII,std::min(val1,1.0));
            m_phd[k][t]=f0;
        }

        if(flag)
            m_pdh[t][k]=pow(m_ch[k],1.0-m_beta[k])*m_phd[k][t];
        else
        {
            m_pdh[t][k]=pow(m_ch[k],1.0-m_beta[k])*(1.0-m_phd[k][t]);
        }
        m_pdh[t][k]=std::max(EPS_H,m_pdh[t][k]);

    }
    m_pdh[t]=util->normalize(m_pdh[t]);

}


QVector<double> MultiDemensionEBI::calcDeltaMu(QVector<double> data, int k,int t)
{
    QVector<double> deltamu;
    deltamu.fill(0.0,m_dimension);

    for(int d=0;d<m_dimension;d++)
    {
        deltamu[d]=m_alpha[k]*data[d]+(1.0-m_alpha[k])*m_mu[k][d]-m_mu[k][d];

    }

    return deltamu;

}

QVector< QVector<double> > MultiDemensionEBI::calcDeltaDev(QVector<double> data,int k)
{
    QVector< QVector<double> >  deltadev;
    deltadev.clear();
    deltadev.resize(m_dimension);
    for(int d=0;d<m_dimension;d++)
    {
        deltadev[d].fill(0.0,m_dimension);
    }

    QVector< QVector<double> >  variance;
    variance.clear();
    variance.resize(m_dimension);
    for(int d=0;d<m_dimension;d++)
    {
        variance[d].fill(0.0,m_dimension);
    }

    for(int d1=0;d1<m_dimension;d1++)
    {
        double v1=data[d1]-m_mu[k][d1];
        for(int d2=0;d2<m_dimension;d2++)
        {
            double v2=data[d2]-m_mu[k][d2];

            variance[d1][d2]=(1.0-m_alpha[k])*m_dev[k][d1][d2]+m_alpha[k]*v1*v2;
            variance[d1][d2]=std::max(variance[d1][d2],EPSII);
            deltadev[d1][d2]=variance[d1][d2]-m_dev[k][d1][d2];

        }

    }

    return deltadev;

}


