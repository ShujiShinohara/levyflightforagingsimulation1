﻿#include "estimateParameterDis.h"
using namespace std;

EstimateParameterDis::EstimateParameterDis()
{
    init();
}

EstimateParameterDis::EstimateParameterDis(QString filename)
{

    init();


    QVector< QVector<double> > tmpdata=util->readCSV(filename);

    for(int i=0;i<tmpdata.size();i++)
    {

        m_ldata.append((double)tmpdata[i][0]);
    }

    std::sort(m_ldata.begin(), m_ldata.end() );

    int dmax=(int)m_ldata.at(m_ldata.size()-1);
    m_freqData.fill(0.0,dmax+1);

    for(int i=0;i<m_ldata.size();i++)
    {
        int index=(int)m_ldata.at(i);
        m_freqData[index]++;
    }

    //Creation of cumulative frequency distribution of observed data
    ceateCumulativeDistributionFromData();

}

int EstimateParameterDis::totalDataNum()
{
    return (int)m_cumFreqData[0];
}
int EstimateParameterDis::ormoreDataNum(int length)
{
    if(length>=m_cumFreqData.size())
        return 0;

    return (int)m_cumFreqData[length];
}

void EstimateParameterDis::init()
{
    util=new Utility;
    m_ldata.clear();
    m_cumulativeFromData.clear();
    m_freqData.clear();
    m_cumFreqData.clear();

    m_TPxmax=0.0;

    m_TPxmin=0.0;
    m_TPmu=0.0;
    m_TPlikelihood=0.0;
    m_TPAIC=0.0;

    m_EPxmax=0.0;
    m_EPxmin=0.0;
    m_EPlambda=0.0;
    m_EPlikelihood=0.0;
    m_EPAIC=0.0;
}




EstimateParameterDis::~EstimateParameterDis()
{


}


//Find the best exponent for the observed data and calculate the likelihood based on the exponent and the observed data.
double EstimateParameterDis::estimateParameterExponential(double xmin, double *likelihood, double *AIC)
{
    double lambda=0.0;
    *likelihood=0.0;
    *AIC=0.0;

    *likelihood=0.0;

    int m;
    double sum=calcIntervalSum(xmin,&m);


    lambda=log((double)m/sum+1.0);
    *likelihood=m*log(1.0-exp(-lambda))-lambda*sum;

    *AIC=-2.0**likelihood+2.0;

    return lambda;

}


void EstimateParameterDis::dumpData(QString filename,double xmin,double tpxmin,double tpxmax,double lambda,double epxmin,double epxmax, double mu)
{
    QVector<double> tmp;
    tmp.clear();

    QVector< QVector<double> > times;
    times.clear();

    double m=0;
    double tpm=0.0;
    double epm=0.0;
    for(int i=0;i<m_ldata.size();i++)
    {
        if(m_ldata.at(i)>=epxmin)
        {
            m++;
        }
        if(m_ldata.at(i)>=tpxmin)
        {
            tpm++;
        }
        if(m_ldata.at(i)>=epxmin)
        {
            epm++;
        }
    }

    double tpxxx=tpm/m;
    double epxxx=epm/m;
    double kkk=calcZeta(tpxmin,tpxmax,mu);

    for(int i=0;i<m_cumulativeFromData.size();i++)
    {
        double xx=m_cumulativeFromData.at(i).at(0);
        double pval_ep=calcCumulativeDistributionForExponential(xx,lambda,epxmin)*epxxx;
        double pval_tp=calcZeta(xx,tpxmax,mu)/kkk*tpxxx;
        double sval=m_cumulativeFromData.at(i).at(1)/m;
        tmp.clear();
        tmp.append(xx);
        tmp.append(pval_tp);
        tmp.append(pval_ep);
        tmp.append(sval);

        times.append(tmp);
    }

    util->save2(times,filename);

}
void EstimateParameterDis::calcLowerBounderForExponential(double *xmin,double *xmax,double *lambda, double *likelihood,double *AIC,double *minD)
{
    *xmin=0.0;
    *likelihood=0.0;
    *AIC=0.0;
    *lambda=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *xmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);


    for(int i=0;i<m_cumulativeFromData.size()-1;i++)
    {
        double tmplikelihood=0.0;
        double tmpAIC=0.0;
        double tmplambda=0.0;

        double tmpmin=m_cumulativeFromData.at(i).at(0);


        //Find the best exponent for the observed data and calculate the likelihood based on the exponent and the observed data.
        tmplambda=estimateParameterExponential(tmpmin,&tmplikelihood,&tmpAIC);

        //Compare the theoretical cumulative frequency distribution with the cumulative frequency distribution from the observed data; the smaller D, the better.
        double D=calcKolmogorovSmirnovStatisticForExponential(tmplambda,tmpmin,*xmax);


        if(i==0)
        {
            *minD=D;
            *xmin=tmpmin;
            *likelihood=tmplikelihood;
            *AIC=tmpAIC;
            *lambda=tmplambda;

        }
        else {
            if(*minD>D)
            {
                *minD=D;
                *xmin=tmpmin;
                *likelihood=tmplikelihood;
                *AIC=tmpAIC;
                *lambda=tmplambda;

            }
        }
    }

}

double EstimateParameterDis::calcIntervalSum(double xmin, int *m)
{
    QVector<double> x;
    x.clear();
    for(int i=0;i<m_ldata.size();i++)
    {
        double val=(double)m_ldata.at(i);
        if(val>=xmin)
        {
            x.append(val);
        }
    }
    double sum=0.0;
    for(int i=0;i<x.size();i++)
    {
        sum+=(x.at(i)-xmin);
    }

    *m=x.size();

    return sum;

}
double EstimateParameterDis::calcCumulativeDistributionForExponential(double x, double lambda, double xmin)
{
    double val=0.0;

    val=exp(-lambda*(x-xmin));

    return val;
}
double EstimateParameterDis::calcKolmogorovSmirnovStatisticForExponential(double lambda,double xmin, double xmax)
{
    double d=0.0;

    double sval=1.0;
    double pval=1.0;

    double m=0.0;
    for(int i=(int)xmin;i<=(int)xmax;i++)
    {
        m+=m_freqData[i];
    }

    double bb1=m;
    for(int i=(int)xmin+1;i<=(int)xmax;i++)
    {
        pval=calcCumulativeDistributionForExponential((double)i,lambda,xmin);

        bb1=bb1-m_freqData[i-1];
        sval=bb1/m;

        double val=fabs(sval-pval);
        if(val>d)
        {
            d=val;
        }

    }

    return d;
}



double EstimateParameterDis::estimateParameterTruncatedPowerlaw(double xmin, double xmax, double *likelihood, double *AIC)
{
    double mu=0.0;
    *likelihood=0.0;
    *AIC=0.0;


    if(xmin>=xmax)
    {
        qDebug()<<"xmin>=xmax";
        return mu;
    }

    if((int)xmax>=m_freqData.size())
    {
        qDebug()<<"xmin>=xmax";
        return mu;
    }


    double sum=0.0;
    double n=0.0;
    for(int i=(int)xmin;i<=(int)xmax;i++)
    {
        if(m_freqData[i]>0.0)
        {
            n+=(double)m_freqData[i];
            sum+=(double)m_freqData[i]*std::log((double)i);
        }
    }


    double lmax=0.0;
    double ll=0.0;

    for(int i=1;i<=300;i++)
    {
         //Likelihood calculation for each µ
        double mu1=0.5+(double)i/100.0;
        double zeta=calcZeta(xmin,xmax,mu1);
        ll=-mu1*sum-n*log(zeta);

        if(i==1)
        {
            lmax=ll;
            mu=mu1;
        }
        else {
            if(lmax<ll)
            {
                lmax=ll;
                mu=mu1;
            }
        }
    }

    *likelihood=lmax;


    *AIC=-2.0**likelihood+2.0;

    return mu;

}

double EstimateParameterDis::calcZeta(double xmin, double xmax, double mu)
{
    double zeta=0.0;
    for(int i=(int)xmin;i<=(int)xmax;i++)
    {
        zeta+=pow((double)i,-mu);
    }

    return zeta;
}




double EstimateParameterDis::calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(double mu,double xmin, double xmax)
{
    double d=0.0;

    double sval=1.0;
    double pval=1.0;

    double m=0.0;
    for(int i=(int)xmin;i<=(int)xmax;i++)
    {
        m+=m_freqData[i];
    }

    double kkk=calcZeta(xmin,xmax,mu);
    if(kkk==0.0)
        return d;

    double kk1=kkk;
    double bb1=m;
    for(int i=(int)xmin+1;i<=(int)xmax;i++)
    {
        kk1=kk1-pow((double)(i-1),-mu);
        pval=kk1/kkk;

        bb1=bb1-m_freqData[i-1];
        sval=bb1/m;

        double val=fabs(sval-pval);
        if(val>d)
        {
            d=val;

        }

    }
    return d;
}


void EstimateParameterDis::calcLowerBounderForTruncatedPowerLaw(double xmax,double *xmin,double *mu,double *likelihood,double *AIC,double *minD)
{
    *likelihood=0.0;
    *AIC=0.0;
    *mu=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *minD=0.0;

    for(int i=0;i<m_cumulativeFromData.size()-1;i++)
    {
        double tmplikelihood=0.0;
        double tmpAIC=0.0;
        double tmpmu=0.0;
        double D=0.0;


        double tmpmin=m_cumulativeFromData.at(i).at(0);
        if(xmax <= tmpmin)
            break;

        tmpmu=estimateParameterTruncatedPowerlaw(tmpmin,xmax,&tmplikelihood,&tmpAIC);
        D=calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(tmpmu,tmpmin,xmax);

        if(i==0)
        {
            *minD=D;
            *xmin=tmpmin;

            *likelihood=tmplikelihood;
            *AIC=tmpAIC;
            *mu=tmpmu;

        }
        else {
            if(*minD>D)
            {
                *minD=D;
                *xmin=tmpmin;
                *likelihood=tmplikelihood;
                *AIC=tmpAIC;
                *mu=tmpmu;
            }
        }

    }

}

void EstimateParameterDis::calcBounderForTruncatedPowerLaw(double *xmin, double *xmax,double *mu,double *likelihood,double *AIC,double *minD)
{
    *xmin=0.0;
    *likelihood=0.0;
    *AIC=0.0;
    *mu=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *xmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);
    *minD=0.0;

    double tmplikelihood=0.0;
    double tmpAIC=0.0;
    double tmpmu=0.0;
    double tmpxmin=1.0;
    double tmpxmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);
    double tmpminD=0.0;

    double D=0.0;


    calcLowerBounderForTruncatedPowerLaw(tmpxmax,&tmpxmin, &tmpmu,&tmplikelihood,&tmpAIC, &tmpminD);

    *xmin=tmpxmin;
    *minD=tmpminD;
    *xmax=tmpxmax;
    *likelihood=tmplikelihood;
    *AIC=tmpAIC;
    *mu=tmpmu;


}


void EstimateParameterDis::ceateCumulativeDistributionFromData()
{


    m_cumulativeFromData.clear();
    m_cumFreqData.fill(0.0,m_ldata.at(m_ldata.size()-1)+1);

    double total=(double)m_ldata.size();
    QVector<double> pair;
    pair.clear();
    pair.append(m_ldata.at(0));
    pair.append(total);
    m_cumulativeFromData.append(pair);
    double sum=m_freqData[0]+m_freqData[1];
    m_cumFreqData[0]=total;
    m_cumFreqData[1]=total;

    for(int i=2;i<m_freqData.size();i++)
    {
        if(m_freqData[i]>0)
        {
            pair.clear();
            double val=total-sum;

            pair.append((double)i);
            pair.append(val);
            m_cumulativeFromData.append(pair);
            sum+=m_freqData[i];
        }

        m_cumFreqData[i]=m_cumFreqData[i-1]-m_freqData[i-1];
    }
}


double EstimateParameterDis::calc_mean(std::vector<double> dat)
{
    double mean = 0.0;

    if(dat.size()==0)
        return mean;

    for(int i=0;i<int(dat.size());i++)
    {
        mean+=dat.at(i);
    }
    if(dat.size()>0)
    {
        mean /= double(dat.size());
    }

    return mean;
}



