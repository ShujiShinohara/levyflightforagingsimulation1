﻿#include "utility.h"
using namespace std;


Utility::Utility(int tm)
{

}

Utility::~Utility()
{


}

bool Utility::isExist(std::string filename)
{
    FILE* fp;

    fp = fopen(filename.c_str(), "r" );
    if( fp == NULL ){
        return false;
    }
    else{
        fclose( fp );
        return true;
    }

    return false;
}
QVector< QVector<double> > Utility::readCSV(QString fileName)
{
    QVector< QVector<double> > rt;
    rt.clear();

    QFile file(fileName);
    if (! file.open(QIODevice::ReadOnly)) {

        return rt;
    }
    QTextStream in(&file);



    while (! in.atEnd()) {

        QStringList list=in.readLine().split(",");

        QVector<double> tmp;
        tmp.clear();
        for(int i=0;i<list.size();i++)
        {
            QString str=list.at(i);
            double val=str.toDouble();
            tmp.append(val);
        }

        rt.append(tmp);

    }
    file.close();

    return rt;
}

void Utility::save2(QVector< QVector<double> > ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    std::string sep=" ";
    QFileInfo info(filename);
    if(info.suffix()=="csv")
        sep=",";

    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        for(int j=0;j<ary[i].size();j++)
        {
            if(j>0)
                ss<<sep<<ary[i][j];
            else
                ss<<ary[i][j];
        }
        ss<<std::endl;

    }

    out << ss.str().c_str();
    file.close();

}



