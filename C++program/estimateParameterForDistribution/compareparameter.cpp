#include "compareparameter.h"

CompareParameter::CompareParameter()
{
    m_tpW=0.0;
    m_epW=0.0;
    m_mu=0.0;
    m_lambda=0.0;
    m_xmin=0.0;
    m_xmax=0.0;
    m_isDiscrete=false;

}

CompareParameter::CompareParameter(QString filename,bool isDiscrete)
{
    m_tpW=0.0;
    m_epW=0.0;
    m_mu=0.0;
    m_lambda=0.0;
    m_xmin=0.0;
    m_xmax=0.0;
    m_isDiscrete=isDiscrete;
    if(m_isDiscrete)
    {
        m_estiDis=new EstimateParameterDis(filename);//Discrete version
        m_estiCon=new EstimateParameterCon();//Continuous version
    }
    else
    {
        m_estiDis=new EstimateParameterDis();//Discrete version
        m_estiCon=new EstimateParameterCon(filename);//Continuous version
    }
}

QStringList CompareParameter::compare()
{
    QStringList results;
    results.clear();
    double tplikelihood=0.0;
    double tpAIC=0.0;
    double eplikelihood=0.0;
    double epAIC=0.0;

    double minD1=0.0;
    double minD2=0.0;

    double mu1=0.0;
    double lambda1=0.0;

    double mu2=0.0;
    double lambda2=0.0;

    double tpxmin=0.0;
    double tpxmax=0.0;

    double epxmin=0.0;
    double epxmax=0.0;

    if(m_isDiscrete)
    {
        //Discrete version
        m_estiDis->calcBounderForTruncatedPowerLaw(&tpxmin,&tpxmax,&mu1,&tplikelihood,&tpAIC, &minD1);
        lambda1=m_estiDis->estimateParameterExponential(tpxmin,&eplikelihood,&epAIC);
    }
    else
    {
        //Continuous version
        m_estiCon->calcBounderForTruncatedPowerLaw(&tpxmin,&tpxmax,&mu1,&tplikelihood,&tpAIC, &minD1);
        lambda1=m_estiCon->estimateParameterExponential(tpxmin,&eplikelihood,&epAIC);
    }



    double minAIC=std::min(epAIC,tpAIC);
    double deltaTP=tpAIC-minAIC;
    double deltaEP=epAIC-minAIC;

    double tpW1=exp(-deltaTP/2.0)/(exp(-deltaTP/2.0)+exp(-deltaEP/2.0));
    double epW1=exp(-deltaEP/2.0)/(exp(-deltaTP/2.0)+exp(-deltaEP/2.0));


    if(m_isDiscrete)
    {
        m_estiDis->calcLowerBounderForExponential(&epxmin,&epxmax,&lambda2,&eplikelihood,&epAIC, &minD2);
        mu2=m_estiDis->estimateParameterTruncatedPowerlaw(epxmin,epxmax,&tplikelihood,&tpAIC);
    }
    else
    {
        m_estiCon->calcLowerBounderForExponential(&epxmin,&epxmax,&lambda2,&eplikelihood,&epAIC, &minD2);
        mu2=m_estiCon->estimateParameterTruncatedPowerlaw(epxmin,epxmax,&tplikelihood,&tpAIC);
    }

    minAIC=std::min(epAIC,tpAIC);
    deltaTP=tpAIC-minAIC;
    deltaEP=epAIC-minAIC;

    double tpW2=exp(-deltaTP/2.0)/(exp(-deltaTP/2.0)+exp(-deltaEP/2.0));
    double epW2=exp(-deltaEP/2.0)/(exp(-deltaTP/2.0)+exp(-deltaEP/2.0));


    QString result="";
    if(tpW1>epW1)
    {
        if(tpW2>epW2)
        {
            result="TP";
        }
        else {
            result="unknown";
        }
    }
    else {
        if(tpW2<epW2)
        {
            result="EP";
        }
        else {
            result="unknown";
        }
    }

    if(result=="unknown")
    {
        qDebug()<<"first unknown";
        double D1=0.0;
        double D2=0.0;


        if(m_isDiscrete)
        {
            D1=log(m_estiDis->totalDataNum())/log(m_estiDis->ormoreDataNum((int)tpxmin))*minD1;
            D2=log(m_estiDis->totalDataNum())/log(m_estiDis->ormoreDataNum((int)epxmin))*minD2;
        }
        else
        {
            D1=log(m_estiCon->totalDataNum())/log(m_estiCon->ormoreDataNum(tpxmin))*minD1;
            D2=log(m_estiCon->totalDataNum())/log(m_estiCon->ormoreDataNum(epxmin))*minD2;
        }

        if(D1<D2)
            result="unknownTP";
        else if(D1>D2)
            result="unknownEP";
        else {
            qDebug()<<"second unknown";
            result="unknown";
        }

    }

    m_tpW=0.0;
    m_epW=0.0;
    m_mu=0.0;
    m_lambda=0.0;
    m_xmin=0.0;
    m_xmax=0.0;

    if(result=="TP" || result=="unknownTP")
    {
        m_tpW=tpW1;
        m_epW=epW1;
        m_mu=mu1;
        m_lambda=lambda1;
        m_xmin=tpxmin;
        m_xmax=tpxmax;

    }
    else if(result=="EP" || result=="unknownEP")
    {
        m_tpW=tpW2;
        m_epW=epW2;
        m_mu=mu2;
        m_lambda=lambda2;
        m_xmin=epxmin;
        m_xmax=epxmax;
    }

    qDebug()<<result<<" "<<m_tpW<<" "<<m_epW<<" mu="<<m_mu<<" lambda="<<m_lambda<<" ["<<m_xmin<<", "<<m_xmax<<"]";

    results.append(result);


    results.append(QString::number(tpW1));
    results.append(QString::number(epW1));
    results.append(QString::number(mu1));

    results.append(QString::number(tpxmin));
    results.append(QString::number(tpxmax));

    results.append(QString::number(tpW2));
    results.append(QString::number(epW2));
    results.append(QString::number(lambda2));
    results.append(QString::number(epxmin));
    results.append(QString::number(epxmax));


    if(m_isDiscrete)
    {
        m_estiDis->dumpData("cdf.csv",m_xmin, tpxmin,tpxmax,mu1,epxmin, epxmax, lambda2);
    }
    else
    {

        m_estiCon->dumpData("cdf.csv",m_xmin, tpxmin,tpxmax,mu1,epxmin, epxmax, lambda2);
    }


    return results;

}
