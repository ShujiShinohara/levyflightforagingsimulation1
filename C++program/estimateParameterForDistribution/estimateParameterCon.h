﻿#ifndef EstimateParameterCon_H
#define EstimateParameterCon_H


#include <string>
#include <float.h>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QVector>
#include <QFileInfo>
#include <sstream>
#include <random>

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <map>
#include <functional>
#include <string.h>

#include "utility.h"
class EstimateParameterCon

{

public:

    EstimateParameterCon();
    EstimateParameterCon(QString filename);
    ~EstimateParameterCon();

    int totalDataNum();
    int ormoreDataNum(double length);



    void calcBounderForTruncatedPowerLaw(double *xmin,double *xmax,double *mu,double *likelihood,double *AIC,double *minD);
    void calcLowerBounderForExponential(double *xmin,double *xmax,double *lambda, double *likelihood,double *AIC,double *minD);

    double estimateParameterTruncatedPowerlaw(double xmin, double xmax,double *likelihood, double *AIC);
    double estimateParameterExponential(double xmin, double *likelihood, double *AIC);


    void dumpData(QString filename,double xmin,double tpxmin,double tpxmax,double mu,double epxmin,double epxmax, double lambda);


private:
    Utility *util;
    QVector<double> m_ldata;//Values are sorted in ascending order
    QVector< QVector< double> > m_cumulativeFromData;//Pairs of values and ranks. Values are sorted in ascending order. Ranks are in descending order.

    double calc_mean(std::vector<double> dat);
    double calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(double mu,int indexmin, int indexmax);
    double calcKolmogorovSmirnovStatisticForExponential(double lambda,int indexmin, int indexmax);
    double calcCumulativeDistributionForTruncatedPowerLaw(double x,double mu,double xmin, double xmax);
    double calcCumulativeDistributionForExponential(double x,double lambda,double xmin);
    double calcCumulativeDistributionFromData(double x,double xmin, double xmax);
    void ceateCumulativeDistributionFromData();
    void calcLowerBounderForTruncatedPowerLaw(double xmax,double *xmin,double *mu,double *likelihood,double *AIC,double *minD);


};

#endif // EstimateParameterCon_H
