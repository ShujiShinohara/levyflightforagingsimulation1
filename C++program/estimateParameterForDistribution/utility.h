﻿#ifndef Utility_H
#define Utility_H


#include <string>

#include <time.h>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QVector>
#include <QFileInfo>
#include <sstream>


#include <stdlib.h>
#include <stdio.h>
#include <vector>

#include <string.h>


class Utility

{

public:

    Utility(int tm=-1);
    ~Utility();

    void save2(QVector< QVector<double> > ary, QString filename);
    QVector< QVector<double> > readCSV(QString fileName);
    static bool isExist(std::string filename);
};

#endif // Utility_H
