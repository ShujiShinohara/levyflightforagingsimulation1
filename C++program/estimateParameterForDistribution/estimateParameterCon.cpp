﻿#include "estimateParameterCon.h"
using namespace std;

EstimateParameterCon::EstimateParameterCon()
{
    util=new Utility;
    m_ldata.clear();
    m_cumulativeFromData.clear();

}

EstimateParameterCon::EstimateParameterCon(QString filename)
{
    util=new Utility;
    m_ldata.clear();
    m_cumulativeFromData.clear();

    QVector< QVector<double> > tmpdata=util->readCSV(filename);

    for(int i=0;i<tmpdata.size();i++)
    {
        m_ldata.append((double)tmpdata[i][0]);
    }

    std::sort(m_ldata.begin(), m_ldata.end() );//Sort by ascending order


    ceateCumulativeDistributionFromData();

}




EstimateParameterCon::~EstimateParameterCon()
{


}

int EstimateParameterCon::totalDataNum()
{
    return m_ldata.size();
}

int EstimateParameterCon::ormoreDataNum(double length)
{

    int tmp=m_cumulativeFromData[0][1];
    for(int i=0;i<m_cumulativeFromData.size();i++)
    {
        if(length<m_cumulativeFromData[i][0])
        {
            break;
        }

        tmp=m_cumulativeFromData[i][1];
    }

    return tmp;
}

void EstimateParameterCon::ceateCumulativeDistributionFromData()
{

    //How many pieces are above a certain value?
    m_cumulativeFromData.clear();

    double total=(double)m_ldata.size();
    QVector<double> pair;
    pair.clear();
    pair.append(m_ldata.at(0));
    pair.append(total);
    m_cumulativeFromData.append(pair);

    for(int i=1;i<m_ldata.size();i++)
    {
        total--;
        if(m_ldata.at(i) >m_ldata.at(i-1))
        {
            pair.clear();
            pair.append(m_ldata.at(i));
            pair.append(total);
            m_cumulativeFromData.append(pair);
        }
    }
}

double EstimateParameterCon::estimateParameterExponential(double xmin, double *likelihood, double *AIC)
{
    double lambda=0.0;
    *likelihood=0.0;
    *AIC=0.0;

    QVector<double> x;
    x.clear();
    for(int i=0;i<m_ldata.size();i++)
    {
        double val=(double)m_ldata.at(i);

        if(val>=xmin)
        {
            x.append(val);
        }
    }

    if(x.size()==0)
        return lambda;

    *likelihood=0.0;


    double sum=0.0;
    for(int i=0;i<x.size();i++)
    {
        sum+=(x.at(i)-xmin);
    }
    double m=(double)x.size();

    if(sum!=0.0)
        lambda=m/sum;
    else
        return lambda;


    *likelihood=m*log(lambda)-lambda*sum;


    *AIC=-2.0**likelihood+2.0;

    return lambda;

}

double EstimateParameterCon::estimateParameterTruncatedPowerlaw(double xmin, double xmax, double *likelihood, double *AIC)
{
    double mu=0.0;
    *likelihood=0.0;
    *AIC=0.0;


    if(xmin==xmax)
    {
        qDebug()<<"xmin==xmax";
        return mu;
    }
    QVector<double> x;
    x.clear();
    for(int i=0;i<m_ldata.size();i++)
    {
        double val=(double)m_ldata.at(i);

        if(val>=xmin && val<=xmax)
        {
            x.append(val);
        }
    }

    if(x.size()==0)
        return mu;


    double sum=0.0;
    for(int i=0;i<x.size();i++)
    {
        sum+=std::log(x.at(i));
    }


    double ave=sum/(double)x.size();

    double n=(double)x.size();


    double lmax=0.0;
    double ll=0.0;
    double diff_min=0.0;
    for(int i=1;i<=300;i++)
    {
        //Likelihood calculation for each µ
        double mu1=0.5+(double)i/100.0;
        double para1=-mu1+1.0;
        double aa=-1.0/para1;
        double bb=(pow(xmax,para1)*log(xmax)-pow(xmin,para1)*log(xmin))/(pow(xmax,para1)-pow(xmin,para1));

        double diff=fabs(aa+bb-ave);



        //Likelihood calculation for each µ
        double aaa=pow(xmin,para1)-pow(xmax,para1);
        ll=n*log(mu1-1.0)-n*log(aaa)-mu1*sum;

        if(i==1)
        {
            diff_min=diff;
            lmax=ll;
            mu=mu1;
        }
        else {
            if(diff<diff_min)
            {
                diff_min=diff;
                lmax=ll;
                mu=mu1;
            }
        }
    }

    *likelihood=lmax;


    *AIC=-2.0**likelihood+2.0;

    return mu;

}

double EstimateParameterCon::calcCumulativeDistributionFromData(double x,double xmin, double xmax)
{
    double rt=0.0;
    if(x<xmin)
        x=xmin;

    double subsum=0.0;
    double sum=0.0;

    for(int i=0;i<m_ldata.size();i++)
    {
        double val=m_ldata.at(i);
        if(val>=xmin && val<=xmax)
        {
            sum++;
            if(val>=x)
            {
                subsum++;
            }
        }
    }

    if(sum>0.0)
        rt=subsum/sum;

    return  rt;


}
double EstimateParameterCon::calcCumulativeDistributionForTruncatedPowerLaw(double x,double mu,double xmin, double xmax)
{
    double val=0.0;
    double para=-mu+1.0;
    val=(pow(x,para)-pow(xmax,para))/(pow(xmin,para)-pow(xmax,para));

    return val;
}

double EstimateParameterCon::calcCumulativeDistributionForExponential(double x, double lambda, double xmin)
{
    double val=0.0;

    val=exp(-lambda*(x-xmin));

    return val;
}

double EstimateParameterCon::calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(double mu,int indexmin, int indexmax)
{
    double d=0.0;

    if(m_ldata.size()==0)
        return d;

    double xmin=m_cumulativeFromData.at(indexmin).at(0);
    double xmax=m_cumulativeFromData.at(indexmax).at(0);
    double sum=m_cumulativeFromData.at(indexmin).at(1)-(m_cumulativeFromData.at(indexmax).at(1)-1.0);
    for(int i=indexmin;i<=indexmax;i++)
    {
        double xval=m_cumulativeFromData[i][0];

        if(xval>xmax)
            break;


        double pval=calcCumulativeDistributionForTruncatedPowerLaw(xval,mu,xmin,xmax);
        double sval=m_cumulativeFromData[i][1]/sum;

        double val=fabs(sval-pval);
        if(val>d)
        {
            d=val;
        }


    }


    return d;
}

double EstimateParameterCon::calcKolmogorovSmirnovStatisticForExponential(double lambda,int indexmin, int indexmax)
{
    double d=0.0;

    if(m_ldata.size()==0)
        return d;

    double xx=m_cumulativeFromData.at(indexmin).at(0);
    double sum=m_cumulativeFromData.at(indexmin).at(1)-(m_cumulativeFromData.at(indexmax).at(1)-1.0);

    double xmin=m_cumulativeFromData.at(indexmin).at(0);
    double xmax=m_cumulativeFromData.at(indexmax).at(0);
    double sval=1.0;
    double pval=1.0;

    d=fabs(sval-pval);


    for(int i=indexmin;i<=indexmax;i++)
    {
        xx=m_cumulativeFromData.at(i).at(0);
        if(xx>xmax)
            break;

        pval=calcCumulativeDistributionForExponential(xx,lambda,xmin);
        sval=m_cumulativeFromData.at(i).at(1)/sum;

        double val=fabs(sval-pval);

        if(val>d)
        {
            d=val;

        }

    }
    return d;
}

void EstimateParameterCon::calcLowerBounderForExponential(double *xmin,double *xmax,double *lambda, double *likelihood,double *AIC,double *minD)
{
    *xmin=0.0;
    *likelihood=0.0;
    *AIC=0.0;
    *lambda=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *xmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);

    int indexmax=m_cumulativeFromData.size()-1;

    for(int i=0;i<m_cumulativeFromData.size()-1;i++)
    {
        double tmplikelihood=0.0;
        double tmpAIC=0.0;
        double tmplambda=0.0;

        double tmpmin=m_cumulativeFromData.at(i).at(0);//Varying xmin, fixed xmax.

        //Find the best exponent for the observed data and calculate the likelihood based on the exponent and the observed data.
        tmplambda=estimateParameterExponential(tmpmin,&tmplikelihood,&tmpAIC);


        //Compare the theoretical cumulative frequency distribution with the cumulative frequency distribution from the observed data; the smaller D, the better.
        double D=calcKolmogorovSmirnovStatisticForExponential(tmplambda,i,indexmax);


        if(i==0)
        {

            *minD=D;
            *xmin=tmpmin;
            *likelihood=tmplikelihood;
            *AIC=tmpAIC;
            *lambda=tmplambda;

        }
        else {
            if(*minD>D)
            {

                *minD=D;
                *xmin=tmpmin;
                *likelihood=tmplikelihood;
                *AIC=tmpAIC;
                *lambda=tmplambda;

            }
        }

    }





}

void EstimateParameterCon::calcLowerBounderForTruncatedPowerLaw(double xmax,double *xmin,double *mu,double *likelihood,double *AIC,double *minD)
{
    *likelihood=0.0;
    *AIC=0.0;
    *mu=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *minD=0.0;

    int indexmin=0;
    int indexmax=m_cumulativeFromData.size()-1;

    for(int i=0;i<m_cumulativeFromData.size()-1;i++)
    {

        double tmplikelihood=0.0;
        double tmpAIC=0.0;
        double tmpmu=0.0;
        double D=0.0;


        double tmpxmin=m_cumulativeFromData.at(i).at(0);
        if(xmax <= tmpxmin)
            break;

        //The parameter μ is obtained by fixing xmin and xmax.
        tmpmu=estimateParameterTruncatedPowerlaw(tmpxmin,xmax,&tmplikelihood,&tmpAIC);


        D=calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(tmpmu,i,indexmax);

        if(i==0)
        {
            *minD=D;
            *xmin=tmpxmin;

            *likelihood=tmplikelihood;
            *AIC=tmpAIC;
            *mu=tmpmu;

        }
        else {
            if(*minD>D)
            {
                *minD=D;
                *xmin=tmpxmin;
                *likelihood=tmplikelihood;
                *AIC=tmpAIC;
                *mu=tmpmu;
            }
        }

    }


}


void EstimateParameterCon::calcBounderForTruncatedPowerLaw(double *xmin,double *xmax,double *mu,double *likelihood,double *AIC,double *minD)
{
    *xmin=0.0;

    *likelihood=0.0;
    *AIC=0.0;
    *mu=0.0;
    *xmin=m_cumulativeFromData.at(0).at(0);
    *xmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);


    *minD=0.0;
    double tmplikelihood=0.0;
    double tmpAIC=0.0;
    double tmpmu=0.0;
    double tmpxmin=0.0;
    double tmpminD=0.0;

    double D=0.0;
    double tmpxmax=m_cumulativeFromData.at(m_cumulativeFromData.size()-1).at(0);


    calcLowerBounderForTruncatedPowerLaw(tmpxmax,&tmpxmin, &tmpmu,&tmplikelihood,&tmpAIC, &tmpminD);

    *minD=tmpminD;
    *xmin=tmpxmin;
    *xmax=tmpxmax;
    *likelihood=tmplikelihood;
    *AIC=tmpAIC;
    *mu=tmpmu;

}

void EstimateParameterCon::dumpData(QString filename,double xmin, double tpxmin,double tpxmax,double mu,double epxmin,double epxmax, double lambda)
{
    QVector<double> tmp;
    tmp.clear();

    QVector< QVector<double> > times;
    times.clear();

    double m=0.0;
    double tpm=0.0;
    double epm=0.0;

    for(int i=0;i<m_ldata.size();i++)
    {
        if(m_ldata.at(i)>=xmin)
        {
            m++;
        }
        if(m_ldata.at(i)>=tpxmin)
        {
            tpm++;
        }
        if(m_ldata.at(i)>=epxmin)
        {
            epm++;
        }
    }

    double tpxxx=tpm/m;
    double epxxx=epm/m;

    for(int i=0;i<m_cumulativeFromData.size();i++)
    {
        double xx=m_cumulativeFromData.at(i).at(0);
        double pval_ep=calcCumulativeDistributionForExponential(xx,lambda,epxmin)*epxxx;
        double pval_tp=calcCumulativeDistributionForTruncatedPowerLaw(xx,mu,tpxmin,tpxmax)*tpxxx;
        double sval=m_cumulativeFromData.at(i).at(1)/m;
        tmp.clear();
        tmp.append(xx);
        tmp.append(pval_tp);
        tmp.append(pval_ep);
        tmp.append(sval);

        times.append(tmp);
    }

    util->save2(times,filename);

}

double EstimateParameterCon::calc_mean(std::vector<double> dat)
{
    double mean = 0.0;

    if(dat.size()==0)
        return mean;

    for(int i=0;i<int(dat.size());i++)
    {
        mean+=dat.at(i);
    }
    if(dat.size()>0)
    {
        mean /= double(dat.size());
    }

    return mean;
}




