#ifndef COMPAREPARAMETER_H
#define COMPAREPARAMETER_H
#include "estimateParameterDis.h"
#include "estimateParameterCon.h"

class CompareParameter
{
public:
    CompareParameter();
    CompareParameter(QString filename,bool isDiscrete=false);
    QStringList compare();
private:
    EstimateParameterDis *m_estiDis;
    EstimateParameterCon *m_estiCon;
    double m_tpW;
    double m_epW;
    double m_mu;
    double m_lambda;
    double m_xmin;
    double m_xmax;
    bool m_isDiscrete;
};

#endif // COMPAREPARAMETER_H
