﻿#ifndef EstimateParameterDis_H
#define EstimateParameterDis_H


#include <string>
#include <float.h>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QVector>
#include <QFileInfo>
#include <sstream>
#include <random>

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <map>
#include <functional>
#include <string.h>

#include "utility.h"
class EstimateParameterDis

{

public:

    EstimateParameterDis();
    EstimateParameterDis(QString filename);
    ~EstimateParameterDis();

    int totalDataNum();
    int ormoreDataNum(int length);



    void calcBounderForTruncatedPowerLaw(double *xmin,double *xmax,double *mu,double *likelihood,double *AIC,double *minD);
    void calcLowerBounderForExponential(double *xmin,double *xmax,double *lambda, double *likelihood,double *AIC,double *minD);


    double estimateParameterTruncatedPowerlaw(double xmin, double xmax,double *likelihood, double *AIC);
    double estimateParameterExponential(double xmin,double *likelihood, double *AIC);

    void dumpData(QString filename,double xmin,double tpxmin,double tpxmax,double lambda,double epxmin,double epxmax, double mu);


private:
    Utility *util;
    QVector<double> m_ldata;
    QVector< QVector< double> > m_cumulativeFromData;
    QVector<double> m_freqData;
    QVector<double> m_cumFreqData;

    double m_TPxmax;
    double m_TPxmin;
    double m_TPmu;
    double m_TPlikelihood;
    double m_TPAIC;

    double m_EPxmax;
    double m_EPxmin;
    double m_EPlambda;
    double m_EPlikelihood;
    double m_EPAIC;


    void init();

    double calcIntervalSum(double xmin, int *m);
    double calc_mean(std::vector<double> dat);
    double calcKolmogorovSmirnovStatisticForTruncatedPowerLaw(double mu,double xmin, double xmax);
    double calcKolmogorovSmirnovStatisticForExponential(double lambda,double xmin, double xmax);
    double calcCumulativeDistributionForExponential(double x,double lambda,double xmin);
    void ceateCumulativeDistributionFromData();
    void calcLowerBounderForTruncatedPowerLaw(double xmax,double *xmin,double *mu,double *likelihood,double *AIC,double *minD);

    double calcZeta(double xmin, double xmax, double mu);



};

#endif // EstimateParameter_H
