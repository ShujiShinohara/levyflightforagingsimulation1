#include <QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QDir>
#include <QVector>
#include <sstream>
#include "compareparameter.h"

CompareParameter *comp;
bool save(std::string result_filename,QStringList data,bool isAppend);
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if(0)
    {
        qDebug()<<"start!";
        bool isDiscrete=true;

        QString filename="";

            filename="./scarse_duration_BIB.csv";
//        filename="./abundant_steplength_BIB.csv";
//            filename="./scarse_steplength_BIB.csv";
        //    filename="./scarse_steplength_EM.csv";
        //    filename="./scarse_steplength_BDI.csv";

        comp=new CompareParameter(filename,isDiscrete);

        QStringList results=comp->compare();

        QStringList tmp;
        tmp.clear();
        tmp.append(filename);
        tmp<<results;

        save("results.csv",tmp,true);
    }
    else
    {
        QStringList filenames;
        filenames.clear();
        QDir dir=QDir::current();
        dir.cd("test");

        QFileInfoList infodirs=dir.entryInfoList(QDir::Dirs);
        qDebug()<<"start!";

        qDebug()<<infodirs;

        foreach(QFileInfo info,infodirs)
        {
            if ( (info.fileName() == "..") || (info.fileName() == ".") )
                continue;


            dir.cd(info.fileName());
            QFileInfoList infos=dir.entryInfoList(QDir::Files);
            if(infos.size()==0)
            {
                qDebug()<<"csv file not found";
                continue;
            }

            foreach(QFileInfo info,infos)
            {
                if(info.suffix().toLower()=="csv")
                {
                    filenames.append(info.absoluteFilePath());

                }
            }
        }

        bool isDiscrete=false;
        foreach(QString fname,filenames)
        {

            comp=new CompareParameter(fname,isDiscrete);

            QStringList results=comp->compare();

            QStringList tmp;
            tmp.clear();
            tmp.append(fname);
            tmp<<results;

            save("results.csv",tmp,true);

        }
    }


    qDebug()<<"End";
    return a.exec();
}

bool save(std::string result_filename,QStringList data,bool isAppend)
{


    std::string sep=",";
    std::ofstream out;

    bool exist=Utility::isExist(result_filename);

    if(isAppend)
        out.open( result_filename.c_str(), std::ios::out | std::ios::app );
    else
        out.open( result_filename.c_str());


    if(!exist || !isAppend)
    {
        out <<"filename";
        out<<sep<<"flag";
        out<<sep<<"tp_weight1";
        out<<sep<<"ep_weight1";

        out<<sep<<"mu";
        out<<sep<<"tpxmin";
        out<<sep<<"tpxmax";

        out<<sep<<"tp_weight2";
        out<<sep<<"ep_weight2";
        out<<sep<<"lambda";
        out<<sep<<"epxmin";
        out<<sep<<"epxmax";

        out <<std::endl;
    }

    if(data.size()==0)
        return false;

    qDebug()<<data;
    out<<data[0].toStdString();

    for(int i=1;i<data.size();i++)
    {
        out <<sep<<data[i].toStdString();
    }

    out<<std::endl;



    out.close();


    return true;

}
